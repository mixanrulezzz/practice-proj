<?php

return [
    'email_required' => 'Email адрес обязателен',
    'email_exists' => 'Такого пользователя не существует',
    'email_unique' => 'Такой email адрес уже используется',
    'password_required' => 'Пароль обязателен',
    'password_min' => 'Пароль должен быть минимум :num символов',
    'password_max' => 'Пароль должен быть максимум :num символов',
    'password_incorrect' => 'Неверный пароль',
    'password_confirmed' => 'Пароли не совпадают',
    'name_required' => 'Имя обязательно',
    'name_min' => 'Имя должно быть минимум из :num символов',
    'name_max' => 'Имя должно быть максимум из :num символов',
    'internal_server_error' => 'Внутренняя ошибка сервера',
    'invalid_token' => 'Неправильный токен',
    'token_already_sent' => 'Токен недавно уже был отправлен',
    'token_required' => 'Токен обязателен',
];
