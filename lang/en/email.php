<?php

return [
    'reset_password_title' => 'Reset password',
    'reset_password_link_title' => 'Reset password link',
];
