<?php

return [
    'email_required' => 'Email address is required',
    'email_exists' => 'User doesn\'t exist',
    'email_unique' => 'Email already in use',
    'password_required' => 'Password is required',
    'password_min' => 'Password must be at least :num characters',
    'password_max' => 'Password must be a maximum of :num characters',
    'password_incorrect' => 'Incorrect password',
    'password_confirmed' => 'Password mismatch',
    'name_required' => 'Name is required',
    'name_min' => 'Name must be at least :num characters',
    'name_max' => 'Name must be a maximum of :num characters',
    'internal_server_error' => 'Internal server error',
    'invalid_token' => 'Invalid token',
    'token_already_sent' => 'Token already been sent recently',
    'token_required' => 'Token is required',
];
