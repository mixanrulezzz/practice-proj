@extends('layouts.email')

@section('title')
    {{ __('email.reset_password_title') }}
@endsection

@section('content')
    <a href="{{ $resetPasswordLink ?? '#' }}">
        {{ __('email.reset_password_link_title') }}
    </a>
@endsection