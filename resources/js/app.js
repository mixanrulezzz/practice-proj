import './bootstrap';
import { createApp } from 'vue';
import App from './vue/app.vue';
import Router from './vue/router/router.js'
import { i18nVue } from 'laravel-vue-i18n';
import { library } from '@fortawesome/fontawesome-svg-core'; /* import the fontawesome core */
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'; /* import font awesome icon component */
import { fas } from '@fortawesome/free-solid-svg-icons'; /* import specific icons */

library.add(fas);

createApp(App)
    .component('font-awesome-icon', FontAwesomeIcon)
    .use(Router)
    .use(i18nVue, {
        fallbackLang: 'ru',
        resolve: async lang => {
            const langs = import.meta.glob('../../lang/vue/*.json');
            return await langs[`../../lang/vue/${lang}.json`]();
        }
    })
    .mount('#app');
