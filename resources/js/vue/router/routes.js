import Main from "../pages/Main.vue";
import FAQ from "../pages/FAQ.vue";
import About from "../pages/About.vue";

const routes = [
    {
        path: '/',
        name: 'Main',
        component: Main,
    },
    {
        path: '/faq',
        name: 'FAQ',
        component: FAQ,
    },
    {
        path: '/about',
        name: 'About',
        component: About,
    },
    {
        path: '/reset_password/:resetToken',
        name: 'ResetPassword',
        component: Main,
    },
];

export default routes;
