import {axiosAuth} from "@/axios/auth.js";
import {reactive} from "vue";


export const authHelper = reactive({
    setAuthTokenAndExpiresTime: ({access_token, expires_in}) => {
        if (access_token) {
            localStorage.setItem('jwt', JSON.stringify({token: access_token, expires: Date.now() + expires_in * 1000}));
            authHelper.isAuth = true;
            authHelper.expiredTime = true;
            authHelper.token = true;
            authHelper.needRefresh = true;
        }
    },
    isTokenExpired: () => {
        return authHelper.expiredTime <= Date.now();
    },
    resetJwt: () => {
        localStorage.removeItem('jwt');
        authHelper.isAuth = false;
        authHelper.expiredTime = false;
        authHelper.token = false;
        authHelper.needRefresh = false;
    },
    refreshToken: () => {
        if (authHelper.needRefresh) {
            axiosAuth
                .refresh()
                .then((res) => {
                    authHelper.setAuthTokenAndExpiresTime(res);
                }).catch((err) => {
                    if (err.response.status === 401) {
                        authHelper.resetJwt();
                    }
                });
        }
    },
    get expiredTime() {
        return JSON.parse(localStorage.getItem('jwt'))?.expires ?? 0;
    },
    set expiredTime(newValue) {
        return true;
    },
    get token() {
        return JSON.parse(localStorage.getItem('jwt'))?.token ?? null;
    },
    set token(newValue) {
        return true;
    },
    get needRefresh() {
        return authHelper.token !== null && authHelper.isTokenExpired();
    },
    set needRefresh(newValue) {
        return true;
    },
    get isAuth() {
        return authHelper.token !== null && !authHelper.isTokenExpired();
    },
    set isAuth(newValue) {
        return true;
    }
});