import { trans as t, I18n } from 'laravel-vue-i18n';
import {computed} from "vue";

export default function trans(key, replacements = {}) {
    const keyArray = key.split('.');
    let tString = t(keyArray[0]);

    if (keyArray.length === 1) {
        return tString;
    }

    for (let i = 1; i < keyArray.length; i++) {
        tString = tString[keyArray[i]];
    }

    return computed(() => (new I18n()).makeReplacements(tString, replacements)).value;
}
