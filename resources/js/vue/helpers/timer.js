export default class Timer {
    constructor(callback, delay) {
        this.callback = callback;
        this.start = Date.now();
        this.remaining = delay;
        this.timerId = setTimeout(callback, delay);
    };

    pause() {
        clearTimeout(this.timerId);
        this.timerId = null;
        this.remaining -= Date.now() - this.start;
    };

    resume() {
        if (this.timerId || this.remaining < 0) {
            return;
        }

        this.start = Date.now();
        this.timerId = setTimeout(this.callback, this.remaining);
    };

    reset() {
        clearTimeout(this.timerId);
        this.timerId = null;
    };
}