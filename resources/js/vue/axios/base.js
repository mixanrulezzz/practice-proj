import axios from "axios";
import {authHelper} from "@/helpers/auth.js";

export const axiosApiV1 = axios.create({
    baseURL: 'http://localhost/api/v1',
    timeout: 10000,
});

// auth settings
axiosApiV1.interceptors.request.use((config) => {
    if (authHelper.token) {
        config.headers.authorization = `Bearer ${authHelper.token}`;
    }

    return config;
});

axiosApiV1.interceptors.response.use(config => {
    if (authHelper.token) {
        config.headers.authorization = `Bearer ${authHelper.token}`;
    }

    return config;
});