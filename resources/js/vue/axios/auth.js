import {axiosApiV1} from "./base.js";

export const axiosAuth = {
    login: async ({email, password}) => {
        const { data } = await axiosApiV1.post('/auth/login', {
            email: email,
            password: password,
        });

        return data ?? {};
    },
    logout: async () => {
        const { data } = await axiosApiV1.get('/auth/logout');

        return data ?? {};
    },
    refresh: async () => {
        const { data } = await axiosApiV1.get('/auth/refresh');

        return data ?? {};
    },
    me: async () => {
        const { data } = await axiosApiV1.get('/auth/me');

        return data ?? {};
    },
    registration: async ({email, name, password, password_confirmation}) => {
        const { data } = await axiosApiV1.post('/auth/registration', {
            email, name,
            password, password_confirmation,
        });

        return data ?? {};
    },
    sendResetPasswordLink: async ({email}) => {
        const { data } = await axiosApiV1.post('/auth/send_reset_link', { email });

        return data ?? {};
    },
    checkResetPasswordToken: async ({token}) => {
        const { data } = await axiosApiV1.post('/auth/check_reset_password_token', { token });

        return data ?? {};
    },
    resetPassword: async ({email, token, password, password_confirmation}) => {
        const { data } = await axiosApiV1.post('/auth/reset_password', {
            email, token,
            password, password_confirmation,
        });

        return data ?? {};
    },
};