<?php

namespace App\Providers;

use Illuminate\Database\Connection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        if ($this->app->isLocal()) {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Model::preventLazyLoading(! app()->isProduction());
        Model::preventSilentlyDiscardingAttributes(! app()->isProduction());

        DB::whenQueryingForLongerThan(500, function (Connection $connection) {
            // todo team notification
        });

        Str::macro('maskEmail', function ($email) {
            $showEmailCharsCount = config('auth.show_email_chars_count', 1);

            return Str::replaceMatches('/\*+/', '***',
                Str::mask(
                    $email,
                    '*',
                    -(Str::length($email) - $showEmailCharsCount),
                    Str::position($email, '@') - $showEmailCharsCount
                )
            );
        });
    }
}
