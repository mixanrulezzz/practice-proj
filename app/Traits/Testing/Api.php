<?php

namespace App\Traits\Testing;

/**
 * Класс для настроек при тестировании АПИ
 */
trait Api
{
    abstract public function withHeader(string $name, string $value);

    public function setUpApi()
    {
        $this->withHeader('Accept', 'application/json');
    }
}
