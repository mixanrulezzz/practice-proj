<?php

namespace App\Traits\Testing\Api\V1\Users\Auth;

use App\Http\Controllers\Api\V1\Users\AuthController;
use Illuminate\Testing\TestResponse;

trait Registration
{
    protected array $loginResponseData = [];

    /**
     * @return TestResponse
     */
    abstract public function post($uri, array $data = [], array $headers = []);

    abstract public static function markTestSkipped(string $message = ''): never;

    public function setUpRegistration()
    {
        $response = $this->post(
            route(AuthController::ROUTE_NAME_PREFIX.'registration'),
            static::getRegistrationData()
        );

        if ($response->getStatusCode() !== 200) {
            $this->markTestSkipped('Registration error: '.$response->getContent());
        }

        $this->loginResponseData = json_decode($response->getContent(), true);

        auth()->logout();
    }

    protected static function getRegistrationData(): array
    {
        return [
            'email' => 'example@test.ru',
            'name' => 'test',
            'password' => '123456',
            'password_confirmation' => '123456',
        ];
    }

    public function getUserJWTToken(): string
    {
        return implode(' ', [\Str::ucfirst($this->loginResponseData['token_type']), $this->loginResponseData['access_token']]);
    }
}
