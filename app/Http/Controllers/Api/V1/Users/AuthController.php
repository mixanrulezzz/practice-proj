<?php

namespace App\Http\Controllers\Api\V1\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\User\CheckResetPasswordTokenRequest;
use App\Http\Requests\Api\V1\User\ForgotPasswordRequest;
use App\Http\Requests\Api\V1\User\LoginRequest;
use App\Http\Requests\Api\V1\User\RegistrationRequest;
use App\Http\Requests\Api\V1\User\ResetPasswordRequest;
use App\Models\PasswordResetToken;
use App\Models\User;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;

class AuthController extends Controller
{
    const string ROUTE_NAME_PREFIX = 'api.v1.auth.';

    public function login(LoginRequest $request): JsonResponse
    {
        $credentials = $request->validated();

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['errors' => ['password' => [__('api_v1.password_incorrect')]]], 422);
        }

        return $this->respondWithToken($token);
    }

    public function registration(RegistrationRequest $request): JsonResponse
    {
        $data = $request->validated();

        $user = app()->make(User::class);
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = $data['password'];

        try {
            $user->saveOrFail();
        } catch (\Exception $e) {
            Log::error($e->getMessage().PHP_EOL.$e->getTraceAsString(), $data);

            return response()->json(['message' => __('api_v1.internal_server_error')], 500);
        }

        return $this->login(app()->make(LoginRequest::class));
    }

    public function me(): JsonResponse
    {
        return response()->json(auth()->user());
    }

    public function logout(): JsonResponse
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    public function refresh(): JsonResponse
    {
        try {
            return $this->respondWithToken(auth()->refresh());
        } catch (TokenBlacklistedException $e) {
            Log::error($e->getMessage().PHP_EOL.$e->getTraceAsString(), request()->header('Authorization'));

            return response()->json(['message' => __('api_v1.internal_server_error')], 500);
        } catch (\Exception $e) {
            Log::error($e->getMessage().PHP_EOL.$e->getTraceAsString());

            return response()->json(['message' => __('api_v1.internal_server_error')], 500);
        }
    }

    public function sendResetPasswordLink(ForgotPasswordRequest $request): JsonResponse
    {
        $email = $request->validated('email');

        $status = Password::sendResetLink(
            ['email' => $email]
        );

        return $status === Password::RESET_LINK_SENT
            ? response()->json(['expire' => config('auth.passwords.users.expire')])
            : response()->json(['errors' => ['email' => [__('api_v1.token_already_sent')]]], 422);
    }

    public function checkResetPasswordToken(CheckResetPasswordTokenRequest $request): JsonResponse
    {
        $token = $request->validated('token');

        $resetTokens = PasswordResetToken::get(['email', 'token']);
        $email = '';
        /** @var PasswordResetToken $item */
        foreach ($resetTokens as $item) {
            if (Hash::check($token, $item->token)) {
                $email = $item->email;
            }
        }

        return $email
            ? response()->json(['email' => $email, 'maskedEmail' => Str::maskEmail($email)])
            : response()->json(['errors' => ['token' => [__('api_v1.invalid_token')]]], 422);
    }

    public function resetPassword(ResetPasswordRequest $request)
    {
        $creds = $request->validated();
        $status = Password::reset($creds, function (User $user, string $password) {
            $user->forceFill([
                'password' => Hash::make($password),
            ])->setRememberToken(Str::random(60));

            $user->save();

            event(new PasswordReset($user));
        });

        return $status === Password::PASSWORD_RESET
            ? response()->noContent()
            : response()->json(['errors' => ['token' => [__('api_v1.invalid_token')]]], 422);
    }

    protected function respondWithToken(string $token): JsonResponse
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
        ]);
    }
}
