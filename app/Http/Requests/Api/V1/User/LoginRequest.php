<?php

namespace App\Http\Requests\Api\V1\User;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return auth()->guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'email' => ['required', 'email', 'exists:'.User::class.',email'],
            'password' => ['required', 'string', 'min:6', 'max:20'],
        ];
    }

    public function messages()
    {
        return [
            'email.required' => __('api_v1.email_required'),
            'email.email' => __('api_v1.email_required'),
            'email.exists' => __('api_v1.email_exists'),
            'password.required' => __('api_v1.password_required'),
            'password.string' => __('api_v1.password_required'),
            'password.min' => __('api_v1.password_min', ['num' => 6]),
            'password.max' => __('api_v1.password_max', ['num' => 20]),
        ];
    }
}
