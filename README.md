# Проект для практики по Laravel, Vue и Moonshine

Создатель: Карпов Михаил

### Используемые компоненты

- [Laravel 10](https://laravel.com/)
- [Vue 3](https://vuejs.org/)
- [Tailwind CSS](https://tailwindcss.com/)
- [Moonshine](https://moonshine-laravel.com/)

### Подготовительные действия

Для работы приложения нужно чтобы было установлено следующее:
- docker >= 20.10.16
- docker-compose >= 1.29.2

### Установка

Для установки нужно сделать следующее по порядку:
- Скачать проект в любую папку
- Зайти в проект
- Запустить ```docker run --rm
  -u "$(id -u):$(id -g)"
  -v "$(pwd):/var/www/html"
  -w /var/www/html
  laravelsail/php83-composer:latest
  composer install --ignore-platform-reqs``` и ждать загрузки нужных пакетов
- Создать файл .env и скопировать туда данные из .env.example
- Настроить файл .env, если требуется
- Запустить команду ```./vendor/bin/sail up``` для запуска сервера
- Зайти в контейнер с php
- Выполнить команду ```php artisan key:generate``` для генерации ключа для работы приложения
- Выполнить команду ```php artisan migrate``` для миграций
- Выполнить команду ```npm install``` для установки зависимотей для npm
- Выполнить команду ```npm run build``` для сборки скриптов и стилей
