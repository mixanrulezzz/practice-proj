<?php

use App\Http\Controllers\Api\V1\Users\AuthController;

Route::prefix('auth')
    ->name(AuthController::ROUTE_NAME_PREFIX)
    ->controller(AuthController::class)
    ->group(function () {
        Route::post('login', 'login')->name('login');
        Route::post('logout', 'logout')->name('logout');
        Route::post('registration', 'registration')->name('registration');
        Route::post('send_reset_link', 'sendResetPasswordLink')->name('sendResetPasswordLink');
        Route::post('check_reset_password_token', 'checkResetPasswordToken')->name('checkResetPasswordToken');
        Route::post('reset_password', 'resetPassword')->name('resetPassword');

        Route::middleware('auth:api')->group(function () {
            Route::get('refresh', 'refresh')->name('refresh');
            Route::get('me', 'me')->name('me');
        });
    });
