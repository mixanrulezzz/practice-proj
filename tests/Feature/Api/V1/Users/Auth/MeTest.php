<?php

namespace Tests\Feature\Api\V1\Users\Auth;

use App\Http\Controllers\Api\V1\Users\AuthController;
use App\Traits\Testing\Api;
use App\Traits\Testing\Api\V1\Users\Auth\Registration;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use Illuminate\Testing\TestResponse;
use Tests\TestCase;

class MeTest extends TestCase
{
    use Api;
    use RefreshDatabase;
    use Registration;

    const ROUTE_NAME = AuthController::ROUTE_NAME_PREFIX.'me';

    protected function setUp(): void
    {
        parent::setUp();

        cache()->clear();
    }

    public function testSuccess(): void
    {
        /** @var TestResponse $response */
        $response = $this
            ->withHeader('Authorization', $this->getUserJWTToken())
            ->get(route(self::ROUTE_NAME));

        $response->assertOk();
        $response->assertJsonStructure([
            'id', 'name', 'email', 'email_verified_at', 'created_at', 'updated_at',
        ]);
    }

    public function testWrongToken(): void
    {
        $wrongToken = Str::substrReplace($this->getUserJWTToken(), '12345', -5);

        /** @var TestResponse $response */
        $response = $this
            ->withHeader('Authorization', $wrongToken)
            ->get(route(self::ROUTE_NAME));

        $response->assertStatus(401);
        $response->assertJsonStructure(['message']);
    }
}
