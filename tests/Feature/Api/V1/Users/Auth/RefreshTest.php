<?php

namespace Tests\Feature\Api\V1\Users\Auth;

use App\Http\Controllers\Api\V1\Users\AuthController;
use App\Traits\Testing\Api;
use App\Traits\Testing\Api\V1\Users\Auth\Registration;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use Tests\TestCase;

class RefreshTest extends TestCase
{
    use Api;
    use RefreshDatabase;
    use Registration;

    const string ROUTE_NAME = AuthController::ROUTE_NAME_PREFIX.'refresh';

    protected function setUp(): void
    {
        parent::setUp();

        cache()->clear();
    }

    /**
     * A basic feature test example.
     */
    public function testRefreshSuccess(): void
    {
        $response = $this
            ->withHeader('Authorization', $this->getUserJWTToken())
            ->get(route(self::ROUTE_NAME));

        $response->assertOk();
    }

    public function testRefreshWrongToken(): void
    {
        $wrongToken = Str::substrReplace($this->getUserJWTToken(), '12345', -5);

        $response = $this
            ->withHeader('Authorization', $wrongToken)
            ->get(route(self::ROUTE_NAME));

        $response->assertStatus(401);
    }
}
