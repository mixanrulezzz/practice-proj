<?php

namespace Tests\Feature\Api\V1\Users\Auth;

use App\Http\Controllers\Api\V1\Users\AuthController;
use App\Models\User;
use App\Traits\Testing\Api;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery\MockInterface;
use Tests\TestCase;

class RegistrationTest extends TestCase
{
    use Api;
    use RefreshDatabase;

    const ROUTE_NAME = AuthController::ROUTE_NAME_PREFIX.'registration';

    private static $testData = [
        'success' => [
            [
                'email' => 'example@test.ru',
                'name' => 'test',
                'password' => '123456',
                'password_confirmation' => '123456',
            ],
            1,
            '',
        ],
        'bad email' => [
            [
                'email' => 'example@',
                'name' => 'test',
                'password' => '123456',
                'password_confirmation' => '123456',
            ],
            0,
            'email',
        ],
        'bad name' => [
            [
                'email' => 'example@test.ru',
                'name' => 't',
                'password' => '123456',
                'password_confirmation' => '123456',
            ],
            0,
            'name',
        ],
        'bad password' => [
            [
                'email' => 'example@test.ru',
                'name' => 'test',
                'password' => '123',
                'password_confirmation' => '123',
            ],
            0,
            'password',
        ],
        'wrong confirmation password' => [
            [
                'email' => 'example@test.ru',
                'name' => 'test',
                'password' => '123456',
                'password_confirmation' => '1234567',
            ],
            0,
            'password',
        ],
    ];

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->withoutToken();
    }

    /**
     * @dataProvider registrationDataProvider
     */
    public function testRegistration($registrationData, $databaseRowsCount, $errorFieldName)
    {
        $response = $this->post(
            route(self::ROUTE_NAME),
            $registrationData
        );

        if ($errorFieldName === '') {
            $response->assertOk();
            $response->assertJsonStructure([
                'access_token', 'token_type', 'expires_in',
            ]);
        } else {
            $response->assertStatus(422);
            $response->assertJsonValidationErrorFor($errorFieldName);
        }

        $this->assertDatabaseCount(User::class, $databaseRowsCount);
    }

    public function testRegistrationUserAlreadyExists()
    {
        $this->testRegistration(...self::$testData['success']);
        auth()->logout();

        $response = $this->post(
            route(AuthController::ROUTE_NAME_PREFIX.'registration'),
            self::$testData['success'][0]
        );

        $response->assertStatus(422);
        $response->assertJsonValidationErrorFor('email');

        $this->assertDatabaseCount(User::class, 1);
    }

    public function testRegistrationFailSave()
    {
        \Log::spy();

        $this->partialMock(User::class, function (MockInterface $mock) {
            $mock->shouldReceive('saveOrFail')->once()->andThrow(\Exception::class);
        });

        $response = $this->post(
            route(AuthController::ROUTE_NAME_PREFIX.'registration'),
            self::$testData['success'][0]
        );

        $response->assertStatus(500);
        $response->assertJsonStructure(['message']);

        $this->assertDatabaseCount(User::class, 0);

        \Log::shouldHaveReceived('error')->once();
    }

    public static function registrationDataProvider()
    {
        return self::$testData;
    }
}
