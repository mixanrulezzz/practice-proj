<?php

namespace Tests\Feature\Api\V1\Users\Auth;

use App\Http\Controllers\Api\V1\Users\AuthController;
use App\Traits\Testing\Api;
use App\Traits\Testing\Api\V1\Users\Auth\Registration;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class LoginTest extends TestCase
{
    use Api;
    use RefreshDatabase;
    use Registration;

    const ROUTE_NAME = AuthController::ROUTE_NAME_PREFIX.'login';

    /**
     * @dataProvider loginDataProvider
     */
    public function testLogin($loginData, $errorFieldName): void
    {
        $response = $this->post(
            route(self::ROUTE_NAME),
            $loginData
        );

        if ($errorFieldName === '') {
            $response->assertStatus(200);
            $response->assertJsonStructure([
                'access_token', 'token_type', 'expires_in',
            ]);
        } else {
            $response->assertStatus(422);
            $response->assertJsonValidationErrorFor($errorFieldName);
        }
    }

    public static function loginDataProvider()
    {
        return [
            'success' => [
                [
                    'email' => self::getRegistrationData()['email'],
                    'password' => self::getRegistrationData()['password'],
                ],
                '',
            ],
            'bad email' => [
                [
                    'email' => 'example@',
                    'password' => self::getRegistrationData()['password'],
                ],
                'email',
            ],
            'user not exists' => [
                [
                    'email' => self::getRegistrationData()['email'].'test',
                    'password' => self::getRegistrationData()['password'],
                ],
                'email',
            ],
            'bad password' => [
                [
                    'email' => self::getRegistrationData()['email'],
                    'password' => '123',
                ],
                'password',
            ],
            'wrong password' => [
                [
                    'email' => self::getRegistrationData()['email'],
                    'password' => '1234567',
                ],
                'password',
            ],
        ];
    }
}
